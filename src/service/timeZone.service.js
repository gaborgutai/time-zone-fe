import axios from 'axios';
import store from '../store/index';

const axiosInstance = axios.create({});

axiosInstance.interceptors.request.use(
  (config) => {
    const token = store.getState().auth.token;

    config.headers.authorization = `Bearer ${token}`

    return config;
  },
  (error) => Promise.reject(error)
);

export class TimeZoneService {
  getAllTimeZoneList = async () => {
    const timeZomes = await axiosInstance.get('http://localhost:3001/server-time-zones');
    return timeZomes;
  }

  getServerTime = async () => {
    const time = await axiosInstance.get('http://localhost:3001/server-time');
    return time;
  }

  login = async (username) => {
    const token = await axiosInstance.post('http://localhost:3001/login', { username });
    return token;
  }
}