import React from 'react';
import Card from 'react-bootstrap/Card';
import classes from './Pages.module.scss';
import classNames from 'classnames';
import moment from 'moment-timezone';
import { connect } from 'react-redux';

import { TimeZoneService } from '../service/timeZone.service';
import { login } from '../store/auth';

class ServerTime extends React.Component  {
  constructor(props) {
    super(props);
    this.state = {
      time: null,
      intervalId: null
    };
  }

  timeZoneService = new TimeZoneService();

  async componentDidMount() {
    await this.props.login();
    const time = await this.timeZoneService.getServerTime();
    this.setState({time: time.data});
    this.setState({intervalId: setInterval(() => {
      this.setState((prevState) => ({time: moment(prevState.time).add(1, 'seconds')}));
    }, 1000)})
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalId);
  }

  render() {
    return (
      <Card className={classNames(classes.MainCard, 'mx-auto')}>
        <div className='d-flex justify-content-center mt-3 mb-3'>
          <h1>{this.state.time ? moment(this.state.time).format("HH:mm:ss") : 'Loading'}</h1>
        </div>
      </Card>
    );
  }
  
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = {
  login
};

export default connect(mapStateToProps, mapDispatchToProps)(ServerTime);