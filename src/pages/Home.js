import React from 'react';
import Card from 'react-bootstrap/Card';
import classes from './Pages.module.scss';
import classNames from 'classnames';

const Home = () => {
  return (
    <Card className={classNames(classes.MainCard, 'mx-auto')}>
      <div className='d-flex justify-content-center mt-3 mb-3'>
        <h1>This is the home page</h1>
      </div>
    </Card>
  );
}

export default Home;