import React from 'react';
import { connect } from 'react-redux';
import { getAllTimeZoneList } from '../store/timeZone';
import { login } from '../store/auth';
import classNames from 'classnames';
import { Form, Formik, Field } from 'formik';
import * as Yup from 'yup';
import moment from 'moment-timezone';


import Card from 'react-bootstrap/Card';
import classes from './Pages.module.scss';

class TimeZone extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      time: null,
      timeZone: null,
      intervalId: null
    };
  }

  setTimeZone = () => {
    if (this.state.timeZone) {
      this.setState({time: moment().tz(this.state.timeZone).format("HH:mm:ss")});
    }
  }

  async componentDidMount() {
    await this.props.login();
    this.props.getAllTimeZoneList();
    this.setTimeZone();
    this.setState({intervalId: setInterval(() => {
      this.setTimeZone();
    }, 1000)})
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalId);
  }

  onSubmit = (values) => {
    this.setState({timeZone: values['timeZone']}, () => {
      this.setState({time: moment().tz(this.state.timeZone).format("HH:mm:ss")});
    });
  }

  render() {
    return (
      <Card className={classNames(classes.MainCard, 'mx-auto')}>
        <div className='d-flex justify-content-center'>
          <h1>{this.state.time}</h1>
        </div>
        <Formik
          initialValues={
            {
              timeZone: 'Europe/Zurich'
            }
          }
          validateOnChange={false}
          validationSchema={Yup.object().shape({
            timeZone: Yup.string().required()
          })}
          onSubmit={this.onSubmit}
        >
          <Form>
            <div className="mb-3 mt-3">
              <h3 className='text-center'>Select a Time Zone</h3>
            </div>
            <div className='w-50 mx-auto'>
              <Field as='select' className='form-control' name="timeZone">
                {this.props.allTimeZoneList.map(timeZone => {
                  return <option key={timeZone.code} value={timeZone.code}>{timeZone.name}</option>
                })}
              </Field>
            </div>
            <div className="mb-3 mt-3 d-flex justify-content-center">
              <button type="submit">Submit</button>
            </div>
          </Form>
        </Formik>
      </Card>
    );
  }

  
}

const mapStateToProps = (state) => {
  return {
    allTimeZoneList: state.timeZone.allTimeZoneList,
  };
};

const mapDispatchToProps = {
  getAllTimeZoneList,
  login
};

export default connect(mapStateToProps, mapDispatchToProps)(TimeZone);