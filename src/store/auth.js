import { createSlice } from '@reduxjs/toolkit';
import { TimeZoneService } from '../service/timeZone.service';

const timeZoneService = new TimeZoneService();

const authSlice = createSlice({
  name: 'auth',
  initialState: {
    token: null,
    error: null
  },
  reducers: {
    setToken: (state, action) => {
      state.token = action.payload;
    },
    setError: (state, action) => {
      state.error = action.payload;
    },
  },
});

const { reducer, actions } = authSlice;

export const login = () => async (dispatch) => {
  try {

    const token = await timeZoneService.login('tesztelek');
    dispatch(actions.setToken(token.data));

  } catch (error) {
    dispatch(actions.setError(JSON.stringify(error)));

    throw error;
  }
};

export default reducer;