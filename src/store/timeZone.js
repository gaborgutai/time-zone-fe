import { createSlice } from '@reduxjs/toolkit';
import { TimeZoneService } from '../service/timeZone.service';

const timeZoneService = new TimeZoneService();

const timeZoneSlice = createSlice({
  name: 'time-zone',
  initialState: {
    allTimeZoneList: [{name: 'Budapest', code: 'Europe/Budapest'}],
    error: null
  },
  reducers: {
    setTimeZones: (state, action) => {
      state.allTimeZoneList = action.payload;
      state.error = null;
    },
    setError: (state, action) => {
      state.error = action.payload;
    },
  },
});

const { reducer, actions } = timeZoneSlice;

export const getAllTimeZoneList = () => async (dispatch) => {
  try {
    const timeZones = await timeZoneService.getAllTimeZoneList();

    dispatch(actions.setTimeZones(timeZones.data.timeZones));

  } catch (error) {
    dispatch(actions.setError(JSON.stringify(error)));

    throw error;
  }
};

export default reducer;
