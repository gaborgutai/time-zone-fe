import { combineReducers, configureStore } from '@reduxjs/toolkit';

import timeZoneReducer from './timeZone';
import authReducer from './auth';

const rootReducer = combineReducers({
  timeZone: timeZoneReducer,
  auth: authReducer,
});

const store = configureStore({
  reducer: rootReducer,
});

export default store;