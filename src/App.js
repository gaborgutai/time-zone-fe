import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import './App.scss';
import Home from './pages/Home';
import TimeZone from './pages/TimeZone';
import Error from './pages/Error';
import Login from './pages/Login';
import Navbar from './components/Navbar'
import ServerTime from './pages/ServerTime';

const App = () => {
  return (
    <div>
      <Navbar/>
      <Switch>
        <Switch>
          <Route
            path="/login"
            exact
            render={() => <Login/>}
          />
          <Route
            path="/404"
            exact
            render={() => <Error/>}
          />
          <Route
            path="/time-zone"
            exact 
            render={() => <TimeZone/>}
          />
          <Route
            path="/server-time"
            exact 
            render={() => <ServerTime/>}
          />
          <Route
            path="/"
            exact 
            render={() => <Home/>}
          />
          <Redirect to="/404" />
        </Switch>
      </Switch>
    </div>
  );
}

export default App;
