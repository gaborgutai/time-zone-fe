# A FE alkalmazás használata
1) git repository-ból beszerzés
2) npm install a FE alkalmazás fő mappában
3) npm run start

# A BE alkalmazás használata
1) git repository-ból beszerzés
2) MSSQL community edition 8.0.21.0 telepítése, elindítása
3) npm install a BE alkalmazás fő mappában
4) node index.js a BE alkalmazás fő mappában
